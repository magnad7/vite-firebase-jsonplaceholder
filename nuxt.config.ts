// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: false,
  modules: [[
    '@pinia/nuxt',
    {
      autoImports: ['defineStore', 'acceptHMRUpdate'],
    },
  ], "@ant-design-vue/nuxt", 'nuxt-vuefire'],

  vuefire: {
    emulators: {

      enabled: false,
      auth: {
        options: {
          disableWarnings: true,
        },
      },
    },
    auth: {
      enabled: true,
      sessionCookie: false,
    },

    config: {
      apiKey: 'AIzaSyAksHtS_qSUpdUtWwYwg8Fw5stoxGZRISo',
      authDomain: 'my-nuxt-task-auth.firebaseapp.com',
      projectId: 'my-nuxt-task-auth',
      messagingSenderId: '761917801894',
      appId: '1:761917801894:web:5f99f33ea81455da283b67',
    },
  },
  components: true,
  imports: {
    dirs: [
      'stores/**',
      'interfaces/**'
    ],
  },
})