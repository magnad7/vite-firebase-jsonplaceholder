import { defineStore } from 'pinia';

interface UserPayloadInterface {
  username: string;
  password: string;
}
interface Posts {
  id?: Number,
  title: String,
  body?: String,
  userId?: Number
}

export const useCommonStore = (unicStoreName: string) =>
  defineStore(`useCommonStore_${unicStoreName}`, () => {
    //  States
    const authenticated: Ref<Boolean | undefined> = ref(false);
    const loading: Ref<Boolean | undefined> = ref(false);
    const data: Ref<Posts[] | undefined> = ref([]);
    const detail: Ref<Posts | undefined> = ref({})
    const fetchData = async () => {
      try {
        loading.value = true
          const response = await useFetch('https://jsonplaceholder.typicode.com/posts');
        if (response.data.value) {
          data.value = response.data.value
        }
      } catch (error) {
        console.error(error);
      }
      finally{
        loading.value = false
      }
    };
    const postDetail = async (form: Posts) => {
      try {
        loading.value = true
        console.log(form)
        const response = await useFetch('https://jsonplaceholder.typicode.com/posts', {
          method: 'POST',
          headers: {  'Content-type': 'application/json; charset=UTF-8', },
          body: form,
        });
        if (response.data.value) {
          return response.data.value
        }
      } catch (error) {
        console.error(error);
      }
      finally{
        loading.value= false
      }
    };
    const getDetail = async (id: string | number | null) => {
      try {
        loading.value = true
        const response = await useFetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
          method: 'GET',
          headers: {  'Content-type': 'application/json; charset=UTF-8', },
        });
        if (response.data.value) {
          detail.value = response.data.value
        }
      } catch (error) {
        console.error(error);
      }
      finally{
        loading.value= false
      }
    };
    const updateData = async (form: Posts) => {
      try {
        const response = await useFetch(`https://jsonplaceholder.typicode.com/posts/${form.value.id}`, {
          method: 'PATCH',
          headers: {  'Content-type': 'application/json; charset=UTF-8', },
          body: form.value,
        });
        if (response.data.value) {
          return response.data.value
        }
      } catch (error) {
        console.error(error);
      }
    };

        const postDelete = async (id: string| number) => {
      try {
        const response = await useFetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
          method: 'DELETE',
        });
        return response
      } catch (error) {
        console.error(error);
      }
    };


    return {
      // States
      data,
      detail,
      loading,
      // Actions
      fetchData,
      updateData,
      getDetail,
      postDetail,
      postDelete
    };
  })();